#include <unistd.h>
#include <sys/syscall.h>
#include <sys/resource.h>
#include <stdio.h>
#include <errno.h>

int main() {
    struct rusage usage;
    //doing some fake operations to consume some CPU cycles
    int num[100000];
    for(int j = 0; j < 2000; j++) {
        for(int i = 0; i < 100000; i ++){
            num[i] = (i * i % 11) * (num[j] - 1) % 31;
        }
    }
    // syscall func gets syscall's name and its needed parameters and runs the syscall
    long result = syscall(SYS_getrusage, RUSAGE_SELF, &usage);

    if (result != 0) {
        int error = errno;
        printf("An error occurred with error number %d\n", error);
    }
    else{
        long userCPUTimeUsed = usage.ru_utime.tv_usec;
        long systemCPUTimeUsed = usage.ru_stime.tv_usec;
        printf("User space CPU time used by this process is %ld micro seconds\n", userCPUTimeUsed);
        printf("Kernel space CPU time used by this process is %ld micro seconds\n", systemCPUTimeUsed);
    }
    return 0;
}
