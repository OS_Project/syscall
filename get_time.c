#include <unistd.h>
#include <sys/syscall.h>
#include <errno.h>
#include <stdio.h>
#include <sys/time.h>

int main() {
	struct timeval tv;
	struct timezone tz;
	// syscall func gets syscall's name and its needed parameters and runs the syscall
	long result = syscall (SYS_gettimeofday, &tv, &tz);
	if (result != 0)
		printf("Unfortunately an error has occurrd with error code: %ld\n", result);
	else {
		long todaySeconds = tv.tv_sec % (24 * 3600);
		todaySeconds = todaySeconds - tz.tz_minuteswest * 60;
		long hour = todaySeconds / 3600;
		long minute = (todaySeconds - hour * 3600) / 60;
		long second = todaySeconds - hour * 3600 - minute * 60;
		printf("%.2ld:%.2ld:%.2ld\n", hour, minute, second);
	}
	return 0;
}
